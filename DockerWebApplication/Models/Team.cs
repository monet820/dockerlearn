﻿using System.Collections.Generic;

namespace DockerWebApplication.Models
{
    // One team, consist of 5 players.
    public class Team
    {
        // Primary Key
        public int Id { get; set; }

        // Field Values
        public string Name { get; set; }
        public int MatchesPlayed { get; set; }

        // Foreign Keys
        public ICollection<Player> Players { get; set; }

    }
}
