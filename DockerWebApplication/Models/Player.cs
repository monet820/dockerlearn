﻿namespace DockerWebApplication.Models
{
    // Player.
    public class Player
    {
        // Primary Key
        public int Id { get; set; }

        // Field Values.
        public string Alias { get; set; }

        // Foreign Keys.
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public int TeamId { get; set; }
        public virtual Team Team { get; set; }
    }
}
