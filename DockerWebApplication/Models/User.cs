﻿namespace DockerWebApplication.Models
{
    public class User
    {
        // Primary key
        public int Id { get; set; }

        // Field Values.
        // Name of the user
        public string Name { get; set; }
        // Property to show when the user was created.
        public int Created { get; set; }

        // Foreign Keys
        public Player Player { get; set; }
    }
}
