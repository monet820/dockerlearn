﻿using System.Collections.Generic;

namespace DockerWebApplication.Models
{
    // The different roles in a Team.
    public class Role
    {
        // Primary Key
        public int Id { get; set; }
        // Field Values.
        public string Name { get; set; }
        public string Description { get; set; }

        // Foreign Keys
        public ICollection<Player> Players { get; set; }

    }
}
