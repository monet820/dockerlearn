﻿using DockerWebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace DockerWebApplication.Context
{
    public class DockerContext : DbContext
    {
        // Base Constructor.
        public DockerContext(DbContextOptions<DockerContext> options) : base(options)
        { }

        // Field Values.
        public Player Player { get; set; }
        public Role Role { get; set; }
        public Team Team { get; set; }
        public User User { get; set; }

        // Instantiate tables.
        public DbSet<Player> Players { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeding Users.
            modelBuilder.Entity<User>().HasData(new User { Id = 1, Name = "Henrik" });
            modelBuilder.Entity<User>().HasData(new User { Id = 2, Name = "Regine" });
            modelBuilder.Entity<User>().HasData(new User { Id = 3, Name = "Kristian" });
            modelBuilder.Entity<User>().HasData(new User { Id = 4, Name = "Thomas" });

            // Seeding Teams
            modelBuilder.Entity<Team>().HasData(new Team { Id = 1, Name = "Neuro", MatchesPlayed = 0 });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 2, Name = "Faze", MatchesPlayed = 0 });

            // Seeding Players
            modelBuilder.Entity<Player>().HasData(new Player { Id = 1, UserId = 1, RoleId = 3, Alias = "Deetail", TeamId = 1});
            modelBuilder.Entity<Player>().HasData(new Player { Id = 2, UserId = 3, RoleId = 2, Alias = "Kreystian", TeamId = 1 });
            modelBuilder.Entity<Player>().HasData(new Player { Id = 3, UserId = 4, RoleId = 1, Alias = "Sogard", TeamId = 1 });
            modelBuilder.Entity<Player>().HasData(new Player { Id = 4, UserId = 2, RoleId = 2, Alias = "Regga", TeamId = 2 });
            
            // Seeding Roles
            modelBuilder.Entity<Role>().HasData(new Role { Id = 1, Name = "Entry" });
            modelBuilder.Entity<Role>().HasData(new Role { Id = 2, Name = "Lurker" });
            modelBuilder.Entity<Role>().HasData(new Role { Id = 3, Name = "Support" });
        }
    }
}
